public class Mahasiswa {
    private String nama;
    private String nim;
    private Gender gender;
    private int semester;
    private String Jurusan;
    private String Jenjang;

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return this.nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getSemester() {
        return this.semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public String getJurusan() {
        return this.Jurusan;
    }

    public void setJurusan(String Jurusan) {
        this.Jurusan = Jurusan;
    }

    public String getJenjang() {
        return this.Jenjang;
    }

    public void setJenjang(String Jenjang) {
        this.Jenjang = Jenjang;
    }

    public String toString() {
        String result = "Nama\t\t:" + getNama();
        result += "\nNIM\t\t:" + getNim();
        result += "\nJenis Kelamin\t:" + getGender().label;
        result += "\nSemester\t:" + getSemester();
        result += "\nJurusan\t\t:" + getJurusan();
        result += "\nJentang\t\t:" + getJenjang();
        return result;
    }

}
