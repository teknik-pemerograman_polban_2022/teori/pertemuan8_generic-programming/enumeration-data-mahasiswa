import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner kScanner = new Scanner(System.in);

        int jmlMahasiswa = 0;
        Mahasiswa MhsPolban[];

        System.out.print("Masukan jumlah mahasiswa: ");
        jmlMahasiswa = kScanner.nextInt();
        MhsPolban = new Mahasiswa[jmlMahasiswa];
        for (int i = 0; i < jmlMahasiswa; i++) {
            MhsPolban[i] = new Mahasiswa();
            System.out.println("===================================");
            System.out.println("Mahasiswa ke-" + (i + 1));
            System.out.print("Masukan nama\t\t:");
            MhsPolban[i].setNama(kScanner.next());
            System.out.print("Masukan NIM\t\t:");
            MhsPolban[i].setNim(kScanner.next());
            boolean valid = false;
            while (!valid) {
                try {
                    System.out.print("Masukan gender(L/P)\t:");
                    MhsPolban[i].setGender(Gender.valueOf(kScanner.next().toUpperCase()));
                    valid = true;
                } catch (Exception e) {
                    System.out.println("==\nInput tidak sesuai!\nHarap entry L/P\n==");
                }
            }
            System.out.print("Masukan Semester\t:");
            MhsPolban[i].setSemester(kScanner.nextInt());
            System.out.print("Masukan Jenjang\t\t:");
            MhsPolban[i].setJenjang(kScanner.next());
            System.out.print("Masukan Jurusan\t\t:");
            MhsPolban[i].setJurusan(kScanner.next());
            System.out.println("===================================");
        }
        System.out.println("\n\n==Data Mahasiswa Entry==");
        for (int i = 0; i < jmlMahasiswa; i++) {
            System.out.println(MhsPolban[i].toString() + "\n");
        }
        kScanner.close();
    }
}
