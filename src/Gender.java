public enum Gender {
    L("Laki-Laki"),
    P("Perempuan");

    public final String label;

    private Gender(String label) {
        this.label = label;
    }
}
